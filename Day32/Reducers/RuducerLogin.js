const stateLogin = {
  users: "",
  password: "",
  firstName: "poo",
  lastName: "pae",
  isLoading: false
};

export default (state = stateLogin, action) => {
  switch (action.type) {
    case "Login":
      return { users: action.username, isLoading: true , password : action.password};

    default:
      return state;
  }
};
