import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  ImageBackground,
  Image
} from "react-native";
import { WhiteSpace, WingBlank,} from "@ant-design/react-native";
class Editprofile extends Component {
  state = {
    users: "",
    password: "",
    firstName: "poo",
    lastName: "pae"
  };

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.Text}>First Name :</Text>
          <TextInput
            style={styles.input}
            placeholder="First name"
            placeholderTextColor="gray"
          />
        </View>

        <View>
          <Text style={styles.Text}>Last Name :</Text>
          <TextInput
            style={styles.input}
            placeholder="Last name"
            placeholderTextColor="gray"
          />
        </View>
        <View>
          <Button
            onPress={() => {
              this.props.history.push("/Profile");
            }}
            title="Save"
            color="#2980B9"
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
        <View>
            <WhiteSpace/>
            <Button
              onPress={() => {
                this.props.history.push("/Listproduct");
              }}
              title="Cancle"
              color="red"
              accessibilityLabel="Learn more about this purple button"
            />
          </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  backgroundImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: null,
    height: null
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    borderWidth: 1,
    borderRadius: 20,
    height: 40,
    width: 250,
    fontSize: 16,
    paddingLeft: 45,
    margin: 20
    // textAlign:"center"
  },
  Text: {
    fontSize: 40,
    fontWeight: "500",
    margin: 20
  },
  Text: {
    alignItems: "center"
  }
});

export default Editprofile;
