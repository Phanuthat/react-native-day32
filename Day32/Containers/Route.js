import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "../Store/store";
import { Route, NativeRouter, Switch, Redirect } from "react-router-native";
import Login from "./Login";
import Profile from "./Profile";
import Listproduct from "./List";
import Editprofile from "./Editprofile";
import Addproduct from "./Addproduct";

class Router extends Component {
  render() {
    return (
      <Provider store = {store} >
        <NativeRouter>
          <Switch>
            <Route exact path="/Login" component={Login} />
            <Route exact path="/Listproduct" component={Listproduct} />
            <Route exact path="/Profile" component={Profile} />
            <Route exact path="/EditProfile" component={Editprofile} />
            <Route exact path="/Addproduct" component={Addproduct} />
            <Redirect to="Login" />
          </Switch>
        </NativeRouter>
      </Provider>
    );
  }
}

export default Router;
