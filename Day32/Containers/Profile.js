import React, { Component } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import { WhiteSpace, WingBlank, Button } from "@ant-design/react-native";
import Header from "./Header";
import {connect} from 'react-redux';
const mapStateToProps = state => {
  return {
    Login: state.Login
  };
};
class Profile extends Component {
  state = {
    users: "",
    password: "",
    firstName: "",
    lastName: ""
  };
  goEditProfile = () => {
    this.props.history.push("/EditProfile");
  };
  render() {
    return (
      <View style={styles.container}>
        <Header style={styles.corner} />
        <View style={styles.viewText}>
          <Text style={styles.Text}>User : {this.props.Login.users}</Text>
        </View>
        <View style={styles.viewText}>
          <Text style={styles.Text}>{`Firstname : ${this.props.Login.firstName}`}</Text>
        </View>
        <View style={styles.viewText}>
          <Text style={styles.Text}>{`Lastname : ${this.props.Login.lastName}`}</Text>
        </View>
        <View styles={styles.button}>
          <WingBlank>
            <WhiteSpace />
            <Button
              type="primary"
              onPress={this.goEditProfile}
              styles={styles.button}
            >
              Edit Profile
            </Button>
            <WhiteSpace />
          </WingBlank>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1

    // margin: 50
  },
  Text: {
    fontSize: 20,
    fontWeight: "400",
    margin: 2,
    color: "#195B87",
    marginLeft: 50,
    marginTop: 50
  },
  viewText: {
    alignItems: "flex-start"
    // ,margin:50
  },
  button: {
    flexDirection: "column",
    flex: 2,
    padding: 20,
    margin: 20
  },
  corner: {
    position: "absolute",
    top: 10,
    right: 20
  }
});
export default connect(mapStateToProps)(Profile);
